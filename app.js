const express = require('express')
const unirest = require('unirest')
const app = express()
const port = 3000
const swapiUrl = 'https://swapi.dev/api/'

const getData = (url, arr) => {
    return new Promise((resolve, reject) => {
        unirest.get(url)
            .then(async (response) => {
                const data = JSON.parse(response.raw_body)
                arr = arr.concat(data.results);
                if (data.next) {
                    getData(data.next, arr).then(resolve).catch(reject)
                } else {
                    resolve(arr);
                }
            })
            .catch(reject)
    });
}

const getSubData = (urls, arr) => {
    return new Promise((resolve, reject) => {
        unirest.get(urls[0])
            .then((response) => {
                const data = JSON.parse(response.raw_body)
                arr.push(data.name);
                urls.shift()
                if (urls.length) {
                    getSubData(urls, arr).then(resolve).catch(reject)
                } else {
                    resolve(arr);
                }
            })
            .catch(reject)
    });
}

const sorterByNumber = (people, by) => {
    return people.sort((a, b) => {
        const aby = !isNaN(parseFloat(a[by])) ? a[by] : 0
        const bby = !isNaN(parseFloat(b[by])) ? b[by] : 0
        return (parseFloat(aby) > parseFloat(bby) ? 1 : -1)
    });
}


app.get('/people', async (req, res) => {
    const { sortBy } = req.query
    let people = []
    let peopleUrl = swapiUrl + 'people'
    people = await getData(peopleUrl, people)
        .then((people) => {
            return people
        })
        .catch((error) => {
            throw error
        });
    switch (sortBy) {
        case 'name': people = people.sort((a, b) => (a.name > b.name ? 1 : -1))
            break;
        case 'height': people = sorterByNumber(people, sortBy)
            break;
        case 'mass': people = sorterByNumber(people, sortBy)
            break;
    };
    res.json(people);
})

app.get('/planets', async (req, res) => {
    let planets = []
    let planetsUrl = swapiUrl + 'planets'
    planets = await getData(planetsUrl, planets)
        .then((planets) => {
            return planets
        })
        .catch((error) => {
            throw error
        });
    for (planet of planets) {
        if (planet.residents.length) {
            planet.residents = await getSubData(planet.residents, [])
        }
    }
    res.json(planets);
})

app.listen(port, () => {
    console.log(`node-exercise running at http://localhost:${port}`);
})